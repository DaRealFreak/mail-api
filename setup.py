#!/usr/local/bin/python
# coding: utf-8

# Bootstrap installation of Distribute

import os
import re

from setuptools import setup

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


def read_file(file_name):
    file_path = os.path.join(
        os.path.dirname(__file__),
        file_name
    )
    return open(file_path).read()


setup(
    name='mailapi',
    version='0.0.2',
    description="a minimalistic api for the temporary mail provider http://www.getairmail.com",
    long_description=read_file('README.md'),
    author=__author__,
    author_email=re.findall('<(.*)>', __author__)[0],
    url='https://bitbucket.org/DaRealFreak/mail-api',
    license=read_file('LICENSE'),
    namespace_packages=[],
    packages=[u'mailapi'],
    package_dir={'': os.path.dirname(__file__)},
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'ghost, BeautifulSoup',
    ],
    entry_points={
    },
    classifiers=[
        'License :: OSI Approved',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        "Programming Language :: Python",
    ],
)

#!/usr/local/bin/python
# coding: utf-8

from main import create_mail

__version__ = '0.0.1'
__name__ = 'mailapi'
__author__ = 'DaRealFreak <dasbaumchen@web.de>'
__all__ = ['create_mail']

#!/usr/local/bin/python
# coding: utf-8
import hashlib
import time


class Message(object):
    """
    message object to differentiate between html and text mail responses
    """

    def __init__(self, message):
        """
        initializing function

        :param message:
        """
        self._html = message

    def text_with_newlines(self, elem):
        """
        get the text with breaks as newlines

        :param elem:
        :return:
        """
        text = ''
        if not hasattr(elem, "recursiveChildGenerator"):
            return self.html

        for e in elem.recursiveChildGenerator():
            if isinstance(e, basestring):
                text += e.strip()
            elif e.name == 'br':
                text += '\n'
        return text

    @property
    def text(self):
        """
        return text response stripped of all html tags(converted <br/> tags to newlines)

        :return:
        """
        return self.text_with_newlines(self._html)

    @property
    def html(self):
        """
        return html response

        :return:
        """
        return unicode(self._html).strip()


class Mail(object):
    """
    small mail model to give the possibility to mark the mails as read or unread
    """

    _read = False

    def __init__(self, mailer_object, subject, sender, recipient, timestamp, message):
        """
        initializing function

        :param mailer_object:
        :param subject:
        :param sender:
        :param recipient:
        :param timestamp:
        :param message:
        """
        self._mailer = mailer_object
        self._subject = subject
        self._sender = sender
        self._recipient = recipient
        self._timestamp = timestamp
        self._message = Message(message)

    @property
    def timestamp(self):
        """
        return the timestamp

        :return:
        """
        return self._timestamp

    @property
    def date(self):
        """
        return the time as a readable date

        :return:
        """
        return time.ctime(self._timestamp)

    @property
    def message(self):
        """
        return the message

        :return:
        """
        return self._message

    @property
    def sender(self):
        """
        return the sender

        :return:
        """
        return self._sender

    @property
    def recipient(self):
        """
        return the sender

        :return:
        """
        return self._recipient

    @property
    def hash(self):
        """
        hash the content of the mail to ignore duplicates

        :return:
        """
        w = str(self._timestamp) + self._sender.encode('utf-8') + self._message.text.encode('utf-8')
        h = hashlib.md5(w)
        return h.digest().encode('base64')[:6]

    @property
    def is_read(self):
        """
        return is read

        :return:
        """
        return self._read

    def mark_as_read(self):
        """
        mark the mail as read

        :return:
        """
        self._read = True
        self._mailer.update(self)

    def mark_as_unread(self):
        """
        mark the mail as unread

        :return:
        """
        self._read = False
        self._mailer.update(self)


if __name__ == '__main__':
    from bs4 import BeautifulSoup

    m = Mail(object, "Hello World", "diebaumchen@web.de", "test@mail.de", time.time(),
             BeautifulSoup("Hello World", "html.parser"))
    print "Time: %s\nFrom: %s\nBody:\n%s" % (m.date, m.sender, m.message.text)
    print m.message.html
    print m.hash

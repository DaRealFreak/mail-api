#!/usr/local/bin/python
# coding: utf-8
import json
import logging
import re
import time

# noinspection PyPackageRequirements
from bs4 import BeautifulSoup

import session_ghost
from mail import Mail
# noinspection PyUnresolvedReferences
from mailapi.decorators import RunAsynch


class BannedException(Exception):
    pass


class MailAPI(object):
    """
    minimalistic api for www.getairmail.com
    """

    base_url = "http://www.getairmail.com/"
    messages = {}
    history = {}
    instance = None

    def __init__(self):
        """
        initializing function

        :return: void
        """
        self.session = session_ghost.GhostSession()
        self.logger = logging.getLogger()
        res = self.session.open(self.base_url + "random")
        self.url = res.url
        if "banned" in self.url:
            raise BannedException("Max amount of mail addresses reached, please wait a while")
        else:
            self.mail_address = re.findall('<title>Temporary Email ([\w]+@[\w.]+)</title>', res.read())[0]

    def update(self, mail_object):
        """
        update the mail object

        :param mail_object:
        :return:
        """
        if mail_object.hash in self.messages:
            self.logger.debug(u"Updating mail object with hash: {0:s}".format(mail_object.hash))
            self.messages[mail_object.hash] = mail_object

    def get_mail(self):
        """
        returns the currently mail address

        :return:
        """
        return self.mail_address

    def remove_callback(self):
        """
        remove the callback instance

        :return:
        """
        self.instance = None

    #@RunAsynch("MailCallback")
    def set_callback(self, instance, interval=5):
        """
        periodically check the mails and call the given function if a new mail arrives

        :param instance:
        :param interval:
        :return:
        """
        self.instance = instance
        while self.instance:
            received_mails = self.get_messages()
            """:type received_mails: list[Mail]"""
            for mail_object in received_mails:
                self.instance(mail_object)
            time.sleep(interval)

    def get_messages(self):
        """
        refresh and get current messages from the inbox

        :return:
        """
        """
        self.session.evaluate('polling_countdown = 0;')
        """
        json_object = self.session.open(self.base_url + 'u/?t=0&_=' + str(int(time.time()))).read()
        messages = json.loads(json_object)
        response = []
        if 'm' in messages:
            for message in messages['m']['m']:
                if message['u'] in self.history:
                    self.logger.debug(u"Skipping url: {0:s}".format(self.base_url + 'h/' + message['u']))
                    mail_hash = self.history[message['u']]
                else:
                    self.logger.debug(u"Opening url: {0:s}".format(self.base_url + 'h/' + message['u']))
                    message_content = self.session.open(self.base_url + 'h/' + message['u']).read()
                    soup = BeautifulSoup(message_content, "html.parser")
                    message_part = soup.find('td', {'style': 'vertical-align: top', 'width': '%80'})
                    # findChildren would be an alternative but cuts out non-html parts on the same level
                    message_content = BeautifulSoup(u"".join([unicode(content) for content in message_part.contents]),
                                                    "html.parser")
                    recipient = "".join(
                        soup.find('div', {'id': 'page-body-content-inner'}).find('p').getText().split("To:")[1:]
                    )

                    mail_object = Mail(self, message['s'], message['f'], recipient, int(message['r']), message_content)
                    mail_hash = mail_object.hash
                    self.history[message['u']] = mail_hash

                # if the mail hash is already in the saved messages use the original object, else
                # save the new mail object into the messages
                if mail_hash in self.messages:
                    mail_object = self.messages[mail_hash]
                else:
                    self.messages[mail_hash] = mail_object

                # only return the unread messages
                if not mail_object.is_read:
                    response.append(mail_object)
        return response


def create_mail():
    """
    return new class object

    :return:
    """
    try:
        return MailAPI()
    except BannedException:
        return False


if __name__ == '__main__':
    def on_receive_mail(mail_object):
        """
        callback for receiving the mail and printing all informations

        :param mail_object:
        :return:
        """
        print u"Time: {0:s}\nFrom: {1:s}\nTo: {2:s}\nBody:\n{3:s}".format(mail_object.date, mail_object.sender,
                                                                          mail_object.recipient,
                                                                          mail_object.message.text)
        print mail_object.message.html
        mail_object.mark_as_read()


    logging.basicConfig(level=logging.DEBUG)
    mail = create_mail()
    if not mail:
        print u"Max amount of mail addresses reached, please try again later"
    else:
        print mail.get_mail()
        mail.set_callback(on_receive_mail)
        # since the callback is running asynchronous just sleep for 2.5 minutes
        time.sleep(150)

# Mailapi

mailapi is a minimalistic api for the temporary mail provider [getairmail].


### Version
0.0.2

### Dependencies

The mailapi uses only cross-platform available third party libraries to provide functionality on all platforms.

Required:

* [ghost] - PyQt4 based JavaScript able webkit
* [BeautifulSoup] - HTML DOM Parser for Python


### Usage

```sh
$ import mailapi
$ mail = mailapi.create_mail()
$ print mail.get_mail()
$ for received_mail in mail.get_messages():
    print received_mail.sender, received_mail.recipient, received_mail.date, received_mail.message.text, received_mail.message.html
    message.mark_as_read()
```

### Development

Want to contribute? Great!
I'm always glad hearing about bugs or pull requests.

### ToDos

 - nothing here
  
License
----

GPL


   [ghost]: <https://github.com/jeanphix/Ghost.py>
   [BeautifulSoup]: <https://www.crummy.com/software/BeautifulSoup/>
   [getairmail]: <http://www.getairmail.com>
      